import { test } from '@playwright/test';
import { USERS } from 'fixtures/users.fixture';
import { LoginPage } from 'page-objects/login.page';

test.describe('Login', () => {
  test('should login with valid credentials', async ({ page }) => {
    const loginPage = new LoginPage(page);

    await loginPage.waitForPageToBeLoaded();
    await loginPage.navigateTo();
    await loginPage.loginAs(USERS.standardUser.username, USERS.standardUser.password);
    await loginPage.verifyErrorMessageIsNotVisible();
  });

  test('should not login with invalid credentials', async ({ page }) => {
    const loginPage = new LoginPage(page);

    await loginPage.waitForPageToBeLoaded();
    await loginPage.navigateTo();
    await loginPage.loginAs(USERS.lockedOutUser.username, USERS.lockedOutUser.password);
    await loginPage.verifyErrorMessageIsVisible();
    await loginPage.verifyErrorMessageContainsText(loginPage.errorMessages.lockedOutUser);
  });

  test('should not login without credentials', async ({ page }) => {
    const loginPage = new LoginPage(page);

    await loginPage.waitForPageToBeLoaded();
    await loginPage.navigateTo();
    await loginPage.submitLoginForm();
    await loginPage.verifyErrorMessageIsVisible();
    await loginPage.verifyErrorMessageContainsText(loginPage.errorMessages.usernameIsRequired);
  });

  test('should not login without username', async ({ page }) => {
    const loginPage = new LoginPage(page);

    await loginPage.waitForPageToBeLoaded();
    await loginPage.navigateTo();
    await loginPage.loginAs('', USERS.standardUser.password);
    await loginPage.verifyErrorMessageIsVisible();
    await loginPage.verifyErrorMessageContainsText(loginPage.errorMessages.usernameIsRequired);
  });

  test('should not login without password', async ({ page }) => {
    const loginPage = new LoginPage(page);

    await loginPage.waitForPageToBeLoaded();
    await loginPage.navigateTo();
    await loginPage.loginAs(USERS.standardUser.username, '');
    await loginPage.verifyErrorMessageIsVisible();
    await loginPage.verifyErrorMessageContainsText(loginPage.errorMessages.passwordIsRequired);
  });
});
