## Swag Labs Demo QA

Swag Labs Demo Playwright Automation

### Install project dependencies
- `npm install`

### Run tests in CLI mode
- `npm run test`

### Run tests in UI mode
- `npm run test:via:ui`

### Show test report
- `npm run show:test:report`

</br><p align="center">
<img src="test-report.png"/></br>
