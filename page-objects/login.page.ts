import { Page, expect } from '@playwright/test';
import { BasePage } from './base.page';

export class LoginPage extends BasePage {
  constructor(page: Page) {
    super(page);
  }

  selectors = {
    usernameInput: '#user-name',
    passwordInput: '#password',
    loginBtn: '#login-button',
    errorMessageBox: '.error-message-container'
  };

  errorMessages = {
    usernameIsRequired: 'Username is required',
    passwordIsRequired: 'Password is required',
    lockedOutUser: 'Sorry, this user has been locked out'
  };

  async navigateTo() {
    await this.page.goto('/');
  }

  async fillInEmail(username: string) {
    await this.page.locator(this.selectors.usernameInput).fill(username);
  }

  async fillInPassword(password: string) {
    await this.page.locator(this.selectors.passwordInput).fill(password);
  }

  async submitLoginForm() {
    await this.page.locator(this.selectors.loginBtn).click();
  }

  async loginAs(username: string, password: string) {
    await this.fillInEmail(username);
    await this.fillInPassword(atob(password));
    await this.submitLoginForm();
  }

  async verifyErrorMessageIsVisible() {
    await expect(this.page.locator(this.selectors.errorMessageBox)).toBeVisible();
  }

  async verifyErrorMessageIsNotVisible() {
    await expect(this.page.locator(this.selectors.errorMessageBox)).not.toBeVisible();
  }

  async verifyErrorMessageContainsText(text: string) {
    await expect(this.page.locator(this.selectors.errorMessageBox)).toContainText(text);
  }
}
