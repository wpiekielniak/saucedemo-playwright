import { Page } from '@playwright/test';

export abstract class BasePage {
  constructor(readonly page: Page) {
    this.page = page;
  }

  async getUrl() {
    return this.page.url();
  }

  async waitForPageToBeLoaded(): Promise<void> {
    await this.page
      .waitForLoadState('domcontentloaded')
      .then(() => this.page.waitForLoadState('networkidle'));
  }
}
