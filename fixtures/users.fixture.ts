type Users = {
  standardUser: {
    username: string;
    password: string;
  };
  lockedOutUser: {
    username: string;
    password: string;
  };
};

export const USERS: Users = {
  standardUser: {
    username: 'standard_user',
    password: 'c2VjcmV0X3NhdWNl'
  },
  lockedOutUser: {
    username: 'locked_out_user',
    password: 'c2VjcmV0X3NhdWNl'
  }
};
